import 'package:ferit_project_app/models/transaction.dart';
import 'package:flutter/material.dart';
import './widgets/transaction_list.dart';

class searchScreen extends StatefulWidget {
  List<Transaction> transactions;

  searchScreen(this.transactions);

  @override
  _searchScreenState createState() => _searchScreenState();
}

class _searchScreenState extends State<searchScreen> {
  TextEditingController textEditingController = TextEditingController();

  void _deleteTransaction(String id) {
    setState(() {
      widget.transactions.removeWhere((tx) {
        return tx.id == id;
      });
    });
  }

  void _search() {
    setState(() {
      print(textEditingController.text);
    });
  }

  List<Transaction> filterBySearchPattern(List<Transaction> transactions) {
    String searchPattern = textEditingController.text.toLowerCase().trim();
    print(searchPattern);
    if (searchPattern == '') {
      return transactions;
    }

    List<Transaction> filtered_transactions = [];

    for (var i = 0; i < transactions.length; i++) {
      if (transactions[i].title.toLowerCase().trim().contains(searchPattern)) {
        filtered_transactions.add(transactions[i]);
      }
    }
    return filtered_transactions;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Search',
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Card(
              elevation: 6,
              margin: EdgeInsets.all(20),
              child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          controller: textEditingController,
                        ),
                      ),
                      RaisedButton(
                        child: Text('Search'),
                        color: Theme.of(context).primaryColor,
                        textColor: Theme.of(context).textTheme.button.color,
                        onPressed: () => _search(),
                      )
                    ],
                  )),
            ),
            SizedBox(
              height: 20,
            ),
            TransactionList(
                filterBySearchPattern(widget.transactions), _deleteTransaction)
          ],
        ),
      ),
    );
  }
}
