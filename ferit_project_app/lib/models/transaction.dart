import 'dart:async';
import 'dart:io';

class Transaction{
  final String id;
  final String title;
  final double amount;
  final DateTime date;
  final String category;
  File image;

  Transaction({this.id,this.title,this.amount,this.date,this.category,this.image});
}
