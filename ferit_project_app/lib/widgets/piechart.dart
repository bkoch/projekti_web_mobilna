import 'package:ferit_project_app/models/transaction.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';


class Piechart extends StatelessWidget {

  List<Transaction> transactions;

  Piechart(this.transactions);

  double foodSpending=0;
  double generalSpending=0;
  double entertainmentSpending=0;
  double shoppingSpending=0;

  Map<String,double> _calculateCategories(List<Transaction> transactions){
    for(var i=0;i<transactions.length;i++){
      var category=transactions[i].category;
      var amount=transactions[i].amount;
      if(category=='shopping'){
        shoppingSpending+=amount;
      }
      else if(category=='general'){
        generalSpending+=amount;
      }
      else if(category=='entertainment'){
        foodSpending+=amount;
      }
      else if(category=='food'){
        foodSpending+=amount;
      }
    }

    Map<String, double> dataMap = new Map();
    dataMap.putIfAbsent("Shopping", () => shoppingSpending);
    dataMap.putIfAbsent("General", () => generalSpending);
    dataMap.putIfAbsent("Food", () => foodSpending);
    dataMap.putIfAbsent("Entertainment", () => entertainmentSpending);

    return dataMap;
  }



  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      child: PieChart(dataMap: _calculateCategories(transactions),chartRadius: MediaQuery.of(context).size.width ,),
    );
  }
}