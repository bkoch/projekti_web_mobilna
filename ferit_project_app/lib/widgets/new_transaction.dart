import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NewTransaction extends StatefulWidget {
  final Function addTx;

  

  NewTransaction(this.addTx);

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  final _titleController = TextEditingController();
  final _amountController = TextEditingController();
  DateTime _selectedDate;

  List<String> categoryList=['shopping','general','food','entertainemnt'];

  int radioValue=2;
  int groupValue=0;

  void changeRadioValue(int value){
      setState(() {
        radioValue=value;
        groupValue=value;
      });
    }

  void _submitData() {
    if (_amountController.text.isEmpty) {
      return;
    }
    final enteredTitle = _titleController.text;
    final enteredAmount = double.parse(_amountController.text);

    if (enteredTitle.isEmpty || enteredAmount <= 0 || _selectedDate == null ) {
      return; 
    }

    widget.addTx(
      enteredTitle,
      enteredAmount,
      _selectedDate,
      categoryList[groupValue]
    );

    setState(() {
      Navigator.of(context).pop();
    });
  }

  void _presentDatePicker() {
    showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(2019),
            lastDate: DateTime.now())
        .then((pickedDate) {
      if (pickedDate == null) {
        return;
      }

      _selectedDate=pickedDate;
    }); 
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            TextField(
              decoration: InputDecoration(
                labelText: 'Title', 
              ),
              controller: _titleController,
              onSubmitted: (_) => _submitData(),
            ),
            TextField(
              decoration: InputDecoration(
                labelText: 'Amount',
              ),
              controller: _amountController,
              keyboardType: TextInputType.number,
              onSubmitted: (_) => _submitData(),
            ),
            Container(
              height: 70,
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Text(_selectedDate == null
                          ? 'No Date Chosen!'
                          : DateFormat.yMd().format(_selectedDate))),
                  FlatButton(
                    textColor: Theme.of(context).primaryColor,
                    child: Text('Choose Date',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    onPressed:
                        _presentDatePicker, 
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Radio(
                  value: 0,
                  groupValue: groupValue,
                  onChanged: (value) => changeRadioValue(value),
                  activeColor: Colors.blue,
                ),
                Text("General",style: TextStyle(fontSize: 11),),
                Radio(
                  value: 1,
                  groupValue: groupValue,
                  onChanged: (value) =>  changeRadioValue(value),
                  activeColor: Colors.red,
                ),
                Text("shopping",style: TextStyle(fontSize: 11),),
                Radio(
                  value: 2,
                  groupValue: groupValue,
                  onChanged: (value) =>  changeRadioValue(value),
                  activeColor: Colors.green,
                ),
                Text("Food",style: TextStyle(fontSize: 11),),
                Radio(
                  value: 3,
                  groupValue: groupValue,
                  onChanged: (value) =>  changeRadioValue(value),
                  activeColor: Colors.yellow,
                ),
                Text("Entertainment",style: TextStyle(fontSize: 11),),
              ],
            ),
            RaisedButton(
                child: Text('Add'),
                color: Theme.of(context).primaryColor,
                textColor: Theme.of(context).textTheme.button.color,
                onPressed: (_submitData)
                )
          ],
        ),
      ),
    );
  }
}
