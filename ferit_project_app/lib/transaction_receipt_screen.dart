import 'package:ferit_project_app/models/transaction.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class TransactionReceiptScreen extends StatefulWidget {

  Transaction transaction;

  TransactionReceiptScreen(this.transaction);

  @override
  _TransactionReceiptScreenState createState() =>
      _TransactionReceiptScreenState();

}

class _TransactionReceiptScreenState extends State<TransactionReceiptScreen> {


  void _getImage(BuildContext context, ImageSource source, Transaction transaction) {
    ImagePicker.pickImage(source: source, maxWidth: 400.0).then((File newImage) {
      setState(() {
        transaction.image=newImage;

      });
      Navigator.pop(context);
    });
  }


  void _openImagePicker(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: 150.0,
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Text('Pick an Image from:',
                    style: TextStyle(fontWeight: FontWeight.bold)),
                SizedBox(
                  height: 10.0,
                ),
                FlatButton(
                  child: Text('Camera'),
                  onPressed: () {
                    _getImage(context, ImageSource.camera,widget.transaction);
                  },
                ),
                FlatButton(
                  child: Text('Gallery'),
                  onPressed: () {
                    _getImage(context, ImageSource.gallery,widget.transaction);
                  },
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Receipt Photo'),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(height: 10.0),
          widget.transaction.image == null
              ? Text('Please pick an image by clicking the button below.',style: Theme.of(context).textTheme.title)
              : Image.file(
                  widget.transaction.image,
                  fit: BoxFit.cover,
                  alignment: Alignment.topCenter,
                  width: MediaQuery.of(context).size.width, 
                ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          _openImagePicker(context);
        },
      ),
    );
  }
}
